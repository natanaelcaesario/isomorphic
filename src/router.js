import React from "react";
import { Route, Switch } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import App from "./containers/App/App";
import NotFound from "./containers/Page/404";
import Form from "./customApp/components/form";
// const RestrictedRoute = ({ component: Component, isLoggedIn, ...rest }) => (
//   <Route
//     {...rest}
//     render={(props) =>
//       isLoggedIn ?
//       (<Component {...props} />) : (<Redirect to={{
//             pathname: "/signin",
//             state: { from: props.location },
//           }}
//         />
//       )
//     }
//   />
// );
const PublicRoutes = ({ history }) => {
  return (
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/dashboard" component={App} />
        <Route
          exact
          path="/github"
          component={() => {
            window.location.href = "https://github.com/natanaelcaesario";
            return null;
          }}
        />
        <Route path="/portal" component={Form} />
        <Route component={NotFound} />
      </Switch>
    </ConnectedRouter>
  );
};

export default PublicRoutes;
