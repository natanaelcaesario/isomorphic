import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper.js";
import LayoutContent from "../../components/utility/layoutContent";
import Task from "../../containers/Forms/FormsWithValidation/index.js";
export default class form extends Component {
  render() {
    const appHeight = window.innerHeight;
    return (
      <LayoutContentWrapper style={{ height: appHeight }}>
        <LayoutContent>
          <Task />
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
