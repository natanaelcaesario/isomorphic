const options = [
  {
    key: "github",
    label: "sidebar.githubSearch",
    leftIcon: "ion-social-github",
  },
  {
    key: "portal",
    label: "sidebar.guestportal",
    leftIcon: "ion-android-person",
  },
];
export default options;
