import React, { Component } from "react";
import { Input } from "antd";
import Form from "../../../components/uielements/form";
import Checkbox from "../../../components/uielements/checkbox";
import Button from "../../../components/uielements/button";
import Notification from "../../../components/notification";
import info from "../../../components/card/index";
const FormItem = Form.Item;

class FormWIthSubmissionButton extends Component {
  state = {
    confirmDirty: false,
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        Notification(
          "success",
          "You will be directed to your purchase information page",
          JSON.stringify(values),
          console.log(values.email)
        );
      }
    });
  };
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };
  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    };
    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem {...formItemLayout} label="Your Email" hasFeedback>
          {getFieldDecorator("email", {
            rules: [
              {
                required: true,
                message: "Please input your Email!",
              },
            ],
          })(<Input name="email" id="email" />)}
        </FormItem>

        <FormItem {...formItemLayout} label="Your Booking Code" hasFeedback>
          {getFieldDecorator("number", {
            rules: [
              {
                required: true,
                message: "Please input your Booking Number!",
              },
            ],
          })(<Input name="number" id="number" />)}
        </FormItem>
        {/* <FormItem {...formItemLayout} label="Password" hasFeedback>
          {getFieldDecorator("password", {
            rules: [
              {
                required: true,
                message: "Please input your password!",
              },
              {
                validator: this.checkConfirm,
              },
            ],
          })(<Input type="password" />)}
        </FormItem> */}
        {/* <FormItem {...formItemLayout} label="Confirm Password" hasFeedback>
          {getFieldDecorator("confirm", {
            rules: [
              {
                required: true,
                message: "Please confirm your password!",
              },
              {
                validator: this.checkPassword,
              },
            ],
          })(<Input type="password" onBlur={this.handleConfirmBlur} />)}
        </FormItem> */}
        <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator("agreement", {
            valuePropName: "checked",
            rules: [
              {
                message: "You have to agree our terms!",
                required: true,
              },
            ],
          })(
            <Checkbox>
              I have read the <a href="# ">agreement</a>
            </Checkbox>
          )}
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Check your Booking
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const WrappedFormWIthSubmissionButton = Form.create()(FormWIthSubmissionButton);
export default WrappedFormWIthSubmissionButton;
